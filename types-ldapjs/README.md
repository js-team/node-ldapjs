# Installation
> `npm install --save @types/ldapjs`

# Summary
This package contains type definitions for ldapjs (http://ldapjs.org).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/ldapjs.

### Additional Details
 * Last updated: Tue, 02 Nov 2021 21:31:20 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Charles Villemure](https://github.com/cvillemure), [Peter Kooijmans](https://github.com/peterkooijmans), [Pablo Moleri](https://github.com/pmoleri), and [Michael Scott-Nelson](https://github.com/mscottnelson).
